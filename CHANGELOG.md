
## 0.0.16 [03-21-2024]

* Removes AWS reference in readme

See merge request itentialopensource/pre-built-automations/arista-eos-vlan-configuration!16

---

## 0.0.15 [02-05-2024]

* make changes for deprecation

See merge request itentialopensource/pre-built-automations/arista-eos-vlan-configuration!12

---

## 0.0.14 [02-04-2024]

* make changes for deprecation

See merge request itentialopensource/pre-built-automations/arista-eos-vlan-configuration!8

---

## 0.0.13 [05-22-2023]

* Merging pre-release/2022.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/arista-eos-vlan-configuration!5

---

## 0.0.12 [02-14-2022]

* Certify on 2021.2

See merge request itentialopensource/pre-built-automations/arista-eos-vlan-configuration!4

---

## 0.0.11 [07-09-2021]

* Update package.json, README.md files

See merge request itentialopensource/pre-built-automations/arista-eos-vlan-configuration!3

---

## 0.0.10 [03-18-2021]

* Patch/lb 515

See merge request itentialopensource/pre-built-automations/arista-eos-vlan-configuration!2

---

## 0.0.9 [01-21-2021]

* Update README.me

See merge request itentialopensource/pre-built-automations/staging/arista-eos-vlan-configuration!1

---

## 0.0.8 [01-18-2021]

* Update README.me

See merge request itentialopensource/pre-built-automations/staging/arista-eos-vlan-configuration!1

---

## 0.0.7 [01-18-2021]

* Update README.me

See merge request itentialopensource/pre-built-automations/staging/arista-eos-vlan-configuration!1

---

## 0.0.6 [01-18-2021]

* Update README.me

See merge request itentialopensource/pre-built-automations/staging/arista-eos-vlan-configuration!1

---

## 0.0.5 [12-21-2020]

* updated the template to replace the word Artifact

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!3

---

## 0.0.4 [12-18-2020]

* updated the template to replace the word Artifact

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!3

---

## 0.0.3 [12-18-2020]

* updated the template to replace the word Artifact

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!3

---

## 0.0.2 [12-18-2020]

* updated the template to replace the word Artifact

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!3

---

## 0.0.6 [10-15-2020]

* updated the template to replace the word Artifact

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!3

---

## 0.0.5 [08-03-2020]

* Fixed repository.url in Package.json

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!2

---

## 0.0.4 [07-17-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.3 [07-07-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.2 [06-19-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---\n\n\n\n
