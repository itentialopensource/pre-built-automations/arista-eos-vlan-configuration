<!-- This is a comment in md (Markdown) format, it will not be visible to the end user -->
## _Deprecation Notice_
This Pre-Built has been deprecated as of 01-15-2024 and will be end of life on 01-15-2025. The capabilities of this Pre-Built have been replaced by the [Arista - EOS - IAG](https://gitlab.com/itentialopensource/pre-built-automations/arista-eos-iag)

<!-- Update the below line with your Pre-Built name -->
# Arista EOS VLAN Configuration

<!-- Leave TOC intact unless you've added or removed headers -->
## Table of Contents
* [Overview](#overview)
* [Features](#features)
* [Requirements](#requirements)
* [Features](#features)
* [Known Limitations](#known-limitations)
* [How to Install](#how-to-install)
* [How to Run](#how-to-run)
* [Components](#components)
* [Command Templates](#command-templates)
* [Metrics](#metrics)
* [Command Template Results](#command-template-results)
* [Template Diffs](#template-diffs)
* [Test Environment](#test-environment)
* [Additional Information](#additional-information)


## Overview
The Arista EOS VLAN Configuration pre-built consists of several workflows and command templates meant to serve as a template for creation and deletion of VLAN configurations on an Arista EOS device.
This pre-built was designed and tested using the Arista EOS instance. Consequently, this pre-built should add and delete VLAN configuration on similar devices with little to no modifications. As a best practice, we recommend that you use a Command Template for your specific device and software version, as well as review the documentation for this pre-built, to determine if modifications are necessary.
The included pre- and post-checks should be customized to suit the needs of the target device.
<!-- ADD ESTIMATED RUN TIME HERE -->
<!-- e.g. Estimated Run Time: 34 min. -->

_Estimated Run Time_: 3 minutes.

## Main Workflow
Pictured below is the main workflow for this pre-built followed by a numbered breakdown of each activity in the automation with create and delete workflows being similar and the few differences between them are discussed later, 
Below is the explanation of each of the numbered tasks in the workflow respectively,

1) JST task to get the formData like device, vlan-id, name and interface for which the VLAN configuration must be created or deleted.
2) Any error in the JST is shown in this task to be corrected and navigates to the end of the task without running any other tasks in the workflow.
3) A job variable used to differentiate between creating and deleting the VLAN config, the value set to 'present' for creation and 'absent' for deletion of the VLAN configuration used by task 9 from the picture above.
4) A pre-check variable set to "not found in current VLAN database" for creation and "active" for deletion workflows respectively so as to check the equality of the response from running the command template in task 6 from the pic above.
5) A post-check variable set to "not found in current VLAN database" for deletion and "active" for creation workflows respectively so as to check the equality of the response from running the command template in task 13 from the pic above.
6) Runs a command template against a device as a pre-check and also evaluates the expected response from the device.
7) Evaluates if all of the commands on task 6 are passing without any errors or unexpected responses.
8) This task shows the results of the pre-check failure and an option to revert back or continue with the standard path in the workflow.
9) This is the main task of the workflow and does the creation or deletion of VLAN configuration on the EOS device based on the number of inputs like device, vlan-id, name, interface and other variables like state supplied to it.
10) JST to check the length of the error array coming as a response from task 9 to evaluate if there are any errors while running task 9.
11) Evaluates if there are any errors returned from task 9.
12) This task shows the results of the response and errors returned from task 9.
13) Runs a command template against a device as a post-check and also evaluates the expected response from the device.
14) Evaluates if all of the commands on task 13 are passing without any errors or unexpected responses.
15) This task shows the results of the post-check failure and an option to revert back or continue with the standard path in the workflow.
16) Evaluates if zero-touch is true or false.
17) Shows the response difference from running the templates if zero touch is false. Skips if zero-touch is true.

## Features
 Notable features for this pre-built are listed below:
- Performs a creation or deletion of VLAN configuration on an Arista EOS device.
- Has a modular design, with each phase of the upgrade process broken out into separate workflows to allow greater customization and flexibility.
- Provides MOP templates for creation and deletion process.
- Pre and post check command templates can be customized if working with a similar device.
- The pre-check and post-check variables and other job variables on the workflows can be customized based on the expected response from running the configuration commands on the device.
- Shows template diffs between pre- and post-checks.
- Zero touch option set as false displays command template results and all error logging.
- Zero touch option executes automation end-to-end without any manual tasks (with the exception of error handling).


<!-- Unordered list highlighting the most exciting features of the Pre-Built -->
<!-- EXAMPLE -->
<!-- * Automatically checks for device type -->
<!-- * Displays dry-run to user (asking for confirmation) prior to pushing config to the device -->
<!-- * Verifies downloaded file integrity (using md5), will try to download again if failed -->

## Requirements
Pre-requisites to use the Arista EOS VLAN Configuration include:

- Itential Automation Platform
  - ^2021.2
- Device instance
  - An Arista EOS device instance installed and running on IAG.

## Known Limitations
At the time of this writing, this pre-built is currently only capable of running on Ansible devices. Support for NSO will be added in a future update.

## How to Install
To install the Pre-Built:

* Verify you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Requirements](#requirements) section in order to install the Pre-Built. 
* The Pre-Built can be installed from within App-Admin_Essential. Simply search for the name of your desired Pre-Built and click the install button.

After the notification displays to inform you that the pre-built has been installed, you should be able to navigate to Automation Catalog and verify that the Arista EOS VLAN Configuration Automation Catalog item has been installed.

## How to Run
This pre-built assumes that there is an IAG device instance running and available to perform VLAN configuration changes like creation and deletion by the pre-built.
An IAG device instance can be added using the configuration manager. Once the device instance has been added, the device must be displayed inside the dropdown on the JSON form when the respective (create or delete) job is started. 
The job can be started through the Automation Catalog. If the device isn't displayed in the dropdown, a restart of the IAP server should help.

## Zero Touch
This pre-built supports options for limiting or expanding the necessary amount of manual interaction while running:

Zero Touch: Enable zero touch to perform the entire upgrade process without any interaction necessary. You should still monitor the state and progress of the device throughout the upgrade. Likewise, any errors that are encountered will still need your attention in order for them to be handled.
Unchecking the zero-touch option on the form will enable the view of additional error logging and debugging information throughout the upgrade process, including MOP template results for pre- and post-checks.

## Components
This pre-built is comprised of a set of modular components intended to simplify the process of creating and deleting VLAN configuration on the respective Arista EOS Device. At the very least, the pre- and post-checks command templates or the job variables in certain tasks like pre-check-var and post-check-var should be modified to ensure success.

## Pre- and Post-checks
This workflow runs pre- and post-checks on the device. If zero-touch is disabled, command template results will be shown for each of the checks.

## Configure Access VLAN task

This task is responsible for configuring (creating or deleting VLAN configuration) the target device based on the inputs provided through job variables in the workflow.

## Command Templates

This pre-built contains few command templates, which may all be customized. Most of the command templates provided should work out of the box. However, please review all provided command templates and documentation for your device before use.

**Pre- and Post-checks**
 The provided command template contains a few commonly used pre and post configuration checks. This is only intended to be a starting point and is not comprehensive.

## Metrics

**Command Template Results**
If zero-touch is disabled, you will be presented with command template results for pre and post checks and response from the configuration of VLAN on the device.

## Test Environment
This pre-built was tested on an Arista EOS device instance.
All testing was conducted using version 2020.2.1 of the Itential Automation Platform.

## Additional Information
Please use your Itential Customer Success account if you need support when using this Pre-Built.
